# -*- coding: utf-8 -*-
"""Defines fixtures available to all tests."""

# from https://github.com/sloria/cookiecutter-flask/blob/master/%7B%7Bcookiecutter.app_name%7D%7D/tests/conftest.py

import pytest
from webtest import TestApp # http://docs.pylonsproject.org/projects/webtest/en/latest/

from ks2.app import create_app
from ks2.extensions import db as _db
from ks2.extensions import cache, redis
from ks2.config import TestConfig

from tests.factories import UserFactory


@pytest.yield_fixture(scope='function')
def app():
    """An application for the tests."""
    _app = create_app(TestConfig)
    ctx = _app.test_request_context()
    ctx.push()

    yield _app

    ctx.pop()


@pytest.fixture(scope='function')
def testapp(app):
    """A Webtest app."""
    return TestApp(app)


@pytest.yield_fixture(scope='function')
def client(app):
    with app.test_client() as client:
        yield client


@pytest.yield_fixture(scope='function')
def db(app):
    """A database for the tests."""
    _db.app = app
    with app.app_context():
        _db.create_all()

    yield _db

    # Explicitly close DB connection
    _db.session.close()
    _db.drop_all()

    cache.clear() # memcached
    redis.flushdb() # redis


@pytest.fixture
def user(db):
    """A user for the tests."""
    user = UserFactory(password='foopass')
    db.session.commit()
    return user