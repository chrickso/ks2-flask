# -*- coding: utf-8 -*-
"""Factories to help in tests."""

# from https://github.com/sloria/cookiecutter-flask/blob/master/%7B%7Bcookiecutter.app_name%7D%7D/tests/factories.py

from factory import PostGenerationMethodCall, Sequence
from factory.alchemy import SQLAlchemyModelFactory

from ks2.extensions import db
from ks2.models.user_model import UserDB


class BaseFactory(SQLAlchemyModelFactory):
    """Base factory."""

    class Meta:
        """Factory configuration."""

        abstract = True
        sqlalchemy_session = db.session


class UserFactory(BaseFactory):
    """User factory."""

    class Meta:
        """Factory configuration."""

        model = UserDB

    username = Sequence(lambda n: 'user{0}'.format(n))
    # email = Sequence(lambda n: 'user{0}@example.com'.format(n))
    # password = PostGenerationMethodCall('set_password', 'testpass')
    password = 'testpass'
    # active = True
    # creator_ip = Sequence(lambda n: 'user{0}'.format(n))

    