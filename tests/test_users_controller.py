# -*- coding: utf-8 -*-
"""Functional tests using WebTest.
See: http://webtest.readthedocs.org/
"""
from flask import url_for

from ks2.constraints import Constraints
from ks2.messages import Messages
from ks2.models.user_model import UserDB
from ks2.utils.cookie import CookieUtils
from tests.factories import UserFactory

constraint = Constraints()
cookie_util = CookieUtils()
message = Messages()


class TestRoutes:
    """Testing all routes in mod_user.py"""

    def test_routes_return_200(self, client):
        assert client.get(url_for('usersBP.signup')).status_code == 200
        assert client.get(url_for('usersBP.login')).status_code == 200
        
    
    def test_routes_requiring_post_return_405_on_get(self, client):
        assert client.get(url_for('usersBP.logout')).status_code == 405


    def test_routes_should_200_where_generated_page_should_exist(self, client, user):        
        assert client.get(url_for('usersBP.profile', user_username=user.username)).status_code == 200


    def test_routes_should_404_where_pages_should_not_exist(self, client, user):
        assert client.get(url_for('usersBP.profile', user_username='usernotfound')).status_code == 404



class TestLogin:
    """Testing user logging in an out."""

    def test_can_log_in_returns_200(self, user, testapp):
        """an unlogged in user should be able to submit the log in form, become logged in, and land on a working page"""
        # Goes to log-in page
        res = testapp.get(url_for('usersBP.login'))
        # Fills out login form
        form = res.forms['login']
        form['username'] = user.username
        form['password'] = 'foopass'
        # Submits
        res = form.submit().follow()
        assert res.status_code == 200
        assert 'user_id'in cookie_util.decode_flask_cookie(testapp.cookies.get('session'))


    # def test_sees_alert_on_log_out(self, user, testapp):
    #     """Show alert on logout."""
    #     # Goes to log-in page
    #     res = testapp.get(url_for('usersBP.login'))
    #     # Fills out login form in navbar
    #     form = res.forms['login']
    #     form['username'] = user.username
    #     form['password'] = 'foopass'
    #     # Submits
    #     res = form.submit().follow()
    #     res = testapp.post(url_for('usersBP.logout')).follow()
    #     # sees alert
    #     assert 'You are now logged out.' in res


    # def test_sees_error_message_if_password_is_incorrect(self, user, testapp):
    #     """Show error if password is incorrect."""
    #     # Goes to log-in page
    #     res = testapp.get(url_for('usersBP.login'))
    #     # Fills out login form, password incorrect
    #     form = res.forms['login']
    #     form['username'] = user.username
    #     form['password'] = 'wrong'
    #     # Submits
    #     res = form.submit()
    #     # sees error
    #     assert 'error' in res # need to check on this. not sure why it would not read the flash message


    # def test_sees_error_message_if_username_doesnt_exist(self, user, testapp):
    #     """Show error if username doesn't exist."""
    #     # Goes to log-in page
    #     res = testapp.get(url_for('usersBP.login'))
    #     # Fills out login form, password incorrect
    #     form = res.forms['login']
    #     form['username'] = 'unknown'
    #     form['password'] = 'foopass'
    #     # Submits
    #     res = form.submit()
    #     # sees error
    #     assert 'That username/password is not in our system.' in res


class TestSignup:
    """Signup a user."""

    # anyway to test recaptcha failing?

    def test_sign_up_success_sees_user_id_in_session(self, user, testapp):
        """a user should be able to complete the sign-up form, be added to the database, and return as logged-in (user_id in session)"""
        old_count = len(UserDB.query.all())
        # Goes to registration page
        res = testapp.get(url_for('usersBP.signup'))
        # Fills out the form
        form = res.forms['signup']
        form['username'] = 'foobar'
        # form['email'] = 'foo@bar.com'
        form['password'] = 'secret'
        form['password_repeat'] = 'secret'
        # Submits
        res = form.submit().follow()
        # A new user was created and logged in
        assert 'user_id'in cookie_util.decode_flask_cookie(testapp.cookies.get('session'))
        assert res.status_code == 200
        assert len(UserDB.query.all()) == old_count + 1

    # def test_sees_404_if_bot_prevention_field_filled(self, user, testapp):
    #     """Program aborts to 404 if the hidden form field is filled."""
    #     # Goes to registration page
    #     res = testapp.get(url_for('usersBP.signup'))
    #     # Fills out form, but anti-bot field is filled
    #     form = res.forms['signup']
    #     form['username'] = 'foobar'
    #     # form['email'] = 'foo@bar.com'
    #     form['password'] = 'secret'
    #     form['password_repeat'] = 'secret'
    #     form['email_repeat'] = 'anything'
    #     # Submit results in 404
    #     res = form.submit(status=404)
    #     assert res.status_code == 404
    

    # def test_sees_error_message_if_username_too_short(self, user, testapp):
    #     """Show error if provided username was too short."""
    #     # Goes to registration page
    #     res = testapp.get(url_for('usersBP.signup'))
    #     # Fills out form, but username too short
    #     form = res.forms['signup']
    #     too_short_username = "x" * (constraint.USER_USERNAME_CHARS_MIN - 1)
    #     form['username'] = too_short_username
    #     # form['email'] = 'foo@bar.com'
    #     form['password'] = 'secret'
    #     form['password_repeat'] = 'secret'
    #     # Submits
    #     res = form.submit()
    #     # sees error message
    #     assert message.USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_MIN % constraint.USER_USERNAME_CHARS_MIN in res


    # def test_sees_error_message_if_username_too_long(self, user, testapp):
    #     """Show error if provided username was too long."""
    #     # Goes to registration page
    #     res = testapp.get(url_for('usersBP.signup'))
    #     # Fills out form, but username too long
    #     form = res.forms['signup']
    #     too_long_username = "x" * (constraint.USER_USERNAME_CHARS_MAX + 1)
    #     form['username'] = too_long_username
    #     # form['email'] = 'foo@bar.com'
    #     form['password'] = 'secret'
    #     form['password_repeat'] = 'secret'
    #     # Submits
    #     res = form.submit()
    #     # sees error message
    #     assert message.USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_MAX % constraint.USER_USERNAME_CHARS_MAX in res

    # def test_sees_error_message_if_username_contains_unsupported_characters(self, user, testapp):
    #     """Show error if provided username contains unsupported characters."""
    #     # Goes to registration page
    #     res = testapp.get(url_for('usersBP.signup'))
    #     # Fills out form, but username contains unsupport character
    #     form = res.forms['signup']
    #     form['username'] = 'foobar!'
    #     # form['email'] = 'foo@bar.com'
    #     form['password'] = 'secret'
    #     form['password_repeat'] = 'secret'
    #     # Submits
    #     res = form.submit()
    #     # sees error message
    #     assert message.USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_UNSUPPORTED_CHARS in res


    # def test_sees_error_message_if_username_was_not_provided(self, user, testapp):
    #     """Show error if username was not provided."""
    #     # Goes to registration page
    #     res = testapp.get(url_for('usersBP.signup'))
    #     # Fills out form, but does not provide username
    #     form = res.forms['signup']
    #     form['username'] = ''
    #     # form['email'] = 'foo@bar.com'
    #     form['password'] = 'secret'
    #     form['password_repeat'] = 'secret'
    #     # Submits
    #     res = form.submit()
    #     # sees error message
    #     assert message.USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_EMPTY in res


    # def test_sees_error_message_if_username_was_only_spaces(self, user, testapp):
    #     """Show error if username was only spaces."""
    #     # Goes to registration page
    #     res = testapp.get(url_for('usersBP.signup'))
    #     # Fills out form, but does provided username was only spaces
    #     form = res.forms['signup']
    #     form['username'] = '     '
    #     # form['email'] = 'foo@bar.com'
    #     form['password'] = 'secret'
    #     form['password_repeat'] = 'secret'
    #     # Submits
    #     res = form.submit()
    #     # sees error message
    #     assert message.USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_EMPTY in res


    # def test_sees_error_message_if_password_too_short(self, user, testapp):
    #     """Show error if provided password was too short."""
    #     # Goes to registration page
    #     res = testapp.get(url_for('usersBP.signup'))
    #     # Fills out form, but password too short
    #     form = res.forms['signup']
    #     form['username'] = 'foobar'
    #     # form['email'] = 'foo@bar.com'
    #     too_short_password = "x" * (constraint.USER_PASSWORD_CHARS_MIN - 1)
    #     form['password'] = too_short_password
    #     form['password_repeat'] = too_short_password
    #     # Submits
    #     res = form.submit()
    #     # sees error message
    #     assert message.USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_MIN % constraint.USER_PASSWORD_CHARS_MIN in res


    # def test_sees_error_message_if_password_too_long(self, user, testapp):
    #     """Show error if provided password was too long."""
    #     # Goes to registration page
    #     res = testapp.get(url_for('usersBP.signup'))
    #     # Fills out form, but password too long
    #     form = res.forms['signup']
    #     form['username'] = 'foobar'
    #     # form['email'] = 'foo@bar.com'
    #     too_long_password = "x" * (constraint.USER_PASSWORD_CHARS_MAX + 1)
    #     form['password'] = too_long_password
    #     form['password_repeat'] = too_long_password
    #     # Submits
    #     res = form.submit()
    #     # sees error message
    #     assert message.USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_MAX % constraint.USER_PASSWORD_CHARS_MAX in res


    # def test_sees_error_message_if_passwords_repeat_empty(self, user, testapp):
    #     """Show error if passwords repeat not provided."""
    #     # Goes to registration page
    #     res = testapp.get(url_for('usersBP.signup'))
    #     # Fills out form, but passwords repeat not provided
    #     form = res.forms['signup']
    #     form['username'] = 'foobar'
    #     # form['email'] = 'foo@bar.com'
    #     form['password'] = 'secret'
    #     form['password_repeat'] = ''
    #     # Submits
    #     res = form.submit()
    #     # sees error message
    #     assert message.USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_REPEAT_EMPTY in res


    # def test_sees_error_message_if_passwords_and_repeat_dont_match(self, user, testapp):
    #     """Show error if passwords and repeat password don't match."""
    #     # Goes to registration page
    #     res = testapp.get(url_for('usersBP.signup'))
    #     # Fills out form, but passwords don't match
    #     form = res.forms['signup']
    #     form['username'] = 'foobar'
    #     # form['email'] = 'foo@bar.com'
    #     form['password'] = 'secret'
    #     form['password_repeat'] = 'secrets'
    #     # Submits
    #     res = form.submit()
    #     # sees error message
    #     assert message.USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_REPEAT_MISMATCH in res


    # def test_sees_error_message_if_passwords_empty(self, user, testapp):
    #     """Show error if passwords not provided."""
    #     # Goes to registration page
    #     res = testapp.get(url_for('usersBP.signup'))
    #     # Fills out form, but passwords don't match
    #     form = res.forms['signup']
    #     form['username'] = 'foobar'
    #     # form['email'] = 'foo@bar.com'
    #     form['password'] = ''
    #     form['password_repeat'] = ''
    #     # Submits
    #     res = form.submit()
    #     # sees error message
    #     assert message.USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_EMPTY in res


    # def test_sees_error_message_if_user_already_registered(self, user, testapp):
    #     """Show error if user already registered."""
    #     # user_factory = UserFactory()  # A registered user
    #     # user_factory.save()
    #     # Goes to registration page
    #     res = testapp.get(url_for('usersBP.signup'))
    #     # Fills out form, but username is already registered
    #     form = res.forms['signup']
    #     form['username'] = user.username
    #     # form['email'] = 'foo@bar.com'
    #     form['password'] = 'secret'
    #     form['password_repeat'] = 'secret'
    #     # Submits
    #     res = form.submit()
    #     # sees error
    #     assert message.USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_TAKEN in res


    # def test_sees_error_message_if_insert_user_into_db_fails(self, user, testapp):
    #     """Show error if form is correct but system fails to insert user into database."""
    #     # Goes to registration page
    #     res = testapp.get(url_for('usersBP.signup'))
    #     # Fills out form, but username equals a designed in fail
    #     form = res.forms['signup']
    #     form['username'] = 'kstesttofail'
    #     # form['email'] = 'foo@bar.com'
    #     form['password'] = 'foopass'
    #     form['password_repeat'] = 'foopass'
    #     # Submits
    #     res = form.submit()
    #     # sees error message
    #     assert message.USER_FORM_PROCESSING_SIGNUP_FLASH_ERROR_CREATING_USER in res


