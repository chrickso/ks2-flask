# -*- coding: utf-8 -*-

import os

from flask_script import Manager, prompt, prompt_pass, prompt_bool
from flask_rq2.script import RQManager

from ks2.app import create_app
from ks2.extensions import cache, db, redis, rq
from ks2.utils.reset import ResetFunctions
from ks2.utils.serving import MyFancyRequestHandler


manager = Manager(create_app)
reset_func = ResetFunctions()


app = create_app()
project_root_path = os.path.join(os.path.dirname(app.root_path))


@manager.command
def run():
    """Run local server."""

    # dev
    app.run(host='0.0.0.0', request_handler=MyFancyRequestHandler)


@manager.command
def prod_run():
    """Run local server."""

    # # # heroku
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)


@manager.command
def cache_reset():
    cache.clear() # memcached
    redis.flushall() # redis


@manager.command
def reset():
    """Reset database."""

    # wipe existing DB's & memcached
    cache.clear() # memcached
    redis.flushall() # redis
    db.drop_all()
    db.create_all()


    # insert test database entries
    reset_func.create_default_users()

    # insert production database entries


    db.session.commit()
    

manager.add_option('-c', '--config',
                   dest="config",
                   required=False,
                   help="config file")


manager.add_command('rq', RQManager(rq))


if __name__ == "__main__":
    manager.run()
