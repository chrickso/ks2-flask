
############################################
###
### Heroku
###
############################################

    Generate new requirements.txt

        # for whenever a new library is installed/update

        pip freeze > requirements.txt

    Scale heroku:

        heroku ps:scale web=1

    See current scale:

        heroku ps

    View Heroku log files:

        heroku logs

    Get to a terminal inside heroku

        heroku run python

    Promote a database to the DATABASE_URL variable:

        heroku pg:promote (blue)

    Run python commands on heroku:

        #reset database:
        heroku run "python shell.py reset"

    Set a variable on the server:

        heroku config:add PROD=True

    Procfile line to run flask server:

        web: python shell.py prod_run

    Procfile line to run gunicorn ( -w # = # of workers):

        web: gunicorn shell:app -b 0.0.0.0:$PORT -w 1

    Heroku PSQL Console

        heroku pg:psql

    Database Status

        heroku pg:info

    live logs

        heroku logs --tail



############################################
###
### Local / Dev Environment Setup
###
############################################

    cd dropbox/repositories/pjapp
    

    Export DATABASE_URL variable so app knows where local postgres is:

        in terminal:

            export DATABASE_URL=postgres://chris@localhost:5432/ks2

    Run local heroku environment with foreman

        terminal:

            foreman start


    upgrade a pip library

        pip install -U lib


############################################
###
### Installing Memcached (Local)
###
############################################

    1.) Requires homebrew (OSX)
        https://github.com/mxcl/homebrew/wiki/installation

    2.) Following heroku's 'Local Memcache Setup'
        https://devcenter.heroku.com/articles/memcache

            term: brew install memcached

            version installed is 1.4.13

        To run the daemon:

            term: memcached -vv

    3.) Following user comment for libmemcached setup
        http://code.google.com/p/memcached/wiki/NewInstallFromPackage

        term: brew install libmemcached

        version installed is 1.0.8

    4.) Per heroku guide, install 'the de-facto python memcache module': pylibmc

        term: pip install pylibmc

    5.) 

############################################
###
### Memcached (Local)
###
############################################

    stop memcached:

        launchctl unload -w ~/Library/LaunchAgents/homebrew.mxcl.memcached.plist

    start memcached:

        memcached -vv

    flush memcached

        python shell.py cache_reset


############################################
###
### Redis (Local)
###
############################################

    info:

        brew info redis

    stop memcached:

        launchctl unload -w ~/Library/LaunchAgents/homebrew.mxcl.redis.plist

    start redis:

        redis-server /usr/local/etc/redis.conf

    flush all keys:

        redis-cli flushall

    flush memcached + redis

        python shell.py cache_reset
