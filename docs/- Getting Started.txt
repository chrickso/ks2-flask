
// Copy QuickFlask Folder and rename

	delete hidden .git folder (not .gitignore)
	rename all instances of 'pbapp' with new app name



// Create VirtualEnv Environment
// http://virtualenvwrapper.readthedocs.org/en/latest/index.html

	mkvirtualenv quickflask



// Setup Git

	// Sourcetree

		File > New > Create Repository

		make initial commit of all files in working tree

	Setup on bitbucket.org



// Install dependancies

	pip install flask

	pip install flask-script

	pip install flask-sqlalchemy

	pip install flask-mail

	pip install flask-bcrypt

	pip install itsdangerous

	// python-postgres library
	pip install psycopg2



// Create new local database

	with postgres.app running

		term: psql -h localhost

		CREATE DATABASE quickflask;

	install hstore extension:

	in psql:

		\c quickflask;

		CREATE EXTENSION hstore;

		



// Heroku Setup

	// install local dependancies
		pip install gunicorn

		pip install gevent

	pip freeze > requirements.txt

	heroku login

	heroku create

	// rename
	heroku apps:rename newname

	// add database add-on
	heroku addons:add heroku-postgresql:dev

	// get database name
	heroku pg

	// promote database
	heroku pg:promote HEROKU_POSTGRESQL_TEAL

	create heroku git remote in sourcetree
	url: git@heroku.com:pbapp.git

	may need to upload public key:
	heroku keys: add ~/.ssh/id_rsa.pub

	git push heroku master

	// generate the database tables
	heroku run "python shell.py reset"

	heroku config:add PROD=True


	heroku pg:psql
	create extension hstore;


	heroku ps:scale web=1 (if account doesn't already have this)





// memcached setup

	install home brew

	brew install libevent

	brew install libmemcached

	brew install memcached



	// install dependancies

		pip install pylibmc

	// update code

		uncomment mods.utils __init.py cache line

		from pbapp.mods.etc.cache.setup import cache # wherever cache is needed

