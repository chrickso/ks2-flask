

// submit form if a text link with class of 'form-submit-button' is hit

$(document).on('click', 'a.form-submit-button', function(e) {
	e.preventDefault();
	$(this).closest('form').submit();
});