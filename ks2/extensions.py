# -*- coding: utf-8 -*-

from flask_bcrypt import Bcrypt
from flask_caching import Cache
from flask_compress import Compress
# from flask_debugtoolbar import DebugToolbarExtension
from flask_mail import Mail
from flask_recaptcha import ReCaptcha
from flask_redis import Redis
from flask_rq2 import RQ
from flask_seasurf import SeaSurf
from flask_sqlalchemy import SQLAlchemy


bcrypt = Bcrypt()
cache = Cache()
compress = Compress()
# toolbar = DebugToolbarExtension()
mail = Mail()
recaptcha = ReCaptcha()
redis = Redis()
rq = RQ()
csrf = SeaSurf()
db = SQLAlchemy()