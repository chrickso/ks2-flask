# -*- coding: utf-8 -*-

from ks2.constraints import Constraints

class Messages(object):


	# ks2.helpers.user_form_processing
    USER_FORM_PROCESSING_SIGNUP_ERROR_CAPTCHA = u'Captcha verification failed.'
    USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_MIN = u'Usernames must be at least %s characters.' # % constraint.USER_USERNAME_CHARS_MIN
    USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_MAX = u'Maximum username length is %s characters.<br>We have trimmed it down for you.' # % constraint.USER_USERNAME_CHARS_MAX
    USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_UNSUPPORTED_CHARS = u'Supported characters are: a-z, A-Z, 0-9, -, and _.'
    USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_EMPTY = u'A username is required.'
    USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_TAKEN = u'This username is already taken.'
    USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_MIN = u'Passwords need to have at least %s characters.' # % constraint.USER_PASSWORD_CHARS_MIN
    USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_MAX = u'Please limit password to %s characters or less.' # % constraint.USER_PASSWORD_CHARS_MAX
    USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_EMPTY = u'A password is required.'
    USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_REPEAT_EMPTY = u'Repeat password was not provided.'
    USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_REPEAT_MISMATCH = u'Your password and repeat password did not match.'

    USER_FORM_PROCESSING_SIGNUP_FLASH_ERROR_CREATING_USER = u'We experienced an issue creating this user. Please try again.'
    USER_FORM_PROCESSING_SIGNUP_FLASH_ERROR_FORM_ERRORS = u'New user was not created. Please correct the <span class="red">error(s)</span> below.'


    # generic
    LOG_IN_REQUIRED_TO_VIEW_PAGE = u'Login required to view previous page.'


	# notices




	# errors
    