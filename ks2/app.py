# -*- coding: utf-8 -*-

import os
import re

from flask import Flask, g, request, render_template

from ks2.extensions import bcrypt, cache, compress, csrf, db, mail, recaptcha, redis, rq #, toolbar

from ks2.utils.cookie import ItsdangerousSessionInterface

# blueprints
from ks2.controllers.notes_controller import notesBP
from ks2.controllers.pages_controller import pagesBP
from ks2.controllers.users_controller import usersBP

from ks2.config import DefaultConfig, APP_NAME

# nl2br filter
# http://flask.pocoo.org/snippets/28/
from jinja2 import evalcontextfilter, Markup, escape

from ks2.utils.times import TimeUtils

time_util = TimeUtils()



# For import *
__all__ = ['create_app']

DEFAULT_BLUEPRINTS = (
    notesBP,
    pagesBP,
    usersBP
)


def create_app(config=None, app_name=None, blueprints=None):
    """Create a Flask app."""

    if app_name is None:
        app_name = APP_NAME
    if blueprints is None:
        blueprints = DEFAULT_BLUEPRINTS

    app = Flask(app_name)
    configure_app(app, config)
    configure_extensions(app)
    configure_hook(app)
    configure_blueprints(app, blueprints)
    configure_logging(app)
    configure_template_filters(app)
    configure_error_handlers(app)

    return app


def configure_app(app, config):
    """Configure app from object, parameter and env."""

    app.config.from_object(DefaultConfig)
    if config is not None:
        app.config.from_object(config)
    # Override setting by env var without touching codes.
    app.config.from_envvar('ks2_APP_CONFIG', silent=True)


def configure_extensions(app):
    # bcrypt
    bcrypt.init_app(app)
    # cache
    cache.init_app(app)
    # compress
    compress.init_app(app)
    # mail
    mail.init_app(app)
    # recaptcha
    recaptcha.init_app(app)
    # redis
    redis.init_app(app)
    # rq
    rq.init_app(app)
    # seasurf
    csrf.init_app(app)
    # sqlalchemy
    db.init_app(app)
    # DebugToolbar
    # toolbar.init_app(app)


def configure_blueprints(app, blueprints):
    """Configure blueprints inside mods/*/*_controller"""

    try:
        for blueprint in blueprints:
            app.register_blueprint(blueprint)
    except:
        app.register_blueprint(blueprints)


def configure_template_filters(app):
    # pass
    @app.template_filter()
    def pretty_date(value):
        return time_util.pretty_date(value)

    @app.template_filter()
    def imgify(url_str):
        if isinstance(url_str, basestring):
            if url_str.endswith('.jpg') or url_str.endswith('.jpeg') \
              or url_str.endswith('.png') or url_str.endswith('.gif'):
                return u'![](%s)' % url_str
            else:
                return url_str
        else:
            return url_str


        return time.utc_title(value)


def configure_logging(app):
    pass
    # """Configure file(info) and email(error) logging."""

    # if app.debug or app.testing:
    #     # skip debug and test mode.
    #     return

    # import logging
    # from logging.handlers import RotatingFileHandler, SMTPHandler

    # # Set info level on logger, which might be overwritten by handers.
    # app.logger.setLevel(logging.INFO)

    # debug_log = os.path.join(app.root_path, app.config['DEBUG_LOG'])
    # file_handler = logging.handlers.RotatingFileHandler(debug_log, maxBytes=100000, backupCount=10)
    # file_handler.setLevel(logging.DEBUG)
    # file_handler.setFormatter(logging.Formatter(
    #     '%(asctime)s %(levelname)s: %(message)s '
    #     '[in %(pathname)s:%(lineno)d]')
    # )
    # app.logger.addHandler(file_handler)

    # ADMINS = ['imwilsonxu@gmail.com']
    # mail_handler = SMTPHandler(app.config['MAIL_SERVER'],
    #                            app.config['MAIL_USERNAME'],
    #                            ADMINS,
    #                            'O_ops... ks2 failed!',
    #                            (app.config['MAIL_USERNAME'],
    #                             app.config['MAIL_PASSWORD']))
    # mail_handler.setLevel(logging.ERROR)
    # mail_handler.setFormatter(logging.Formatter(
    #     '%(asctime)s %(levelname)s: %(message)s '
    #     '[in %(pathname)s:%(lineno)d]')
    # )
    # app.logger.addHandler(mail_handler)


def configure_hook(app):
    pass
    # @app.before_request
    # def before_request():
    #     pass



def configure_error_handlers(app):
    @app.errorhandler(403)
    def forbidden_page(error):
        return render_template("error_pages/403.html"), 403

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template("error_pages/404.html"), 404

    @app.errorhandler(405)
    def method_not_allowed_page(error):
        return render_template("error_pages/405.html"), 405

    @app.errorhandler(500)
    def server_error_page(error):
        return render_template("error_pages/500.html"), 500
