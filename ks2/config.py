# -*- coding: utf-8 -*-
"""Application configuration."""

import os

APP_NAME = 'ks2'

class BaseConfig(object):

    DEBUG = False
    TESTING = False

    # os.urandom(24)
    SECRET_KEY = 'q\xf2\x92\xc4\x17-c\x1a\x83.k\xb4Z\x96K\xd5\x19|\xa9~\xb0\xbf\x8b\xc8'
    SECURITY_PASSWORD_SALT = 'some_random_salt_plus_a_nonword_wigyusauwhgoskzidn'
    # this is required for subdomains to work
    if os.environ.get('PROD'):
        SERVER_NAME = 'ks2.herokuapp.com'

class DefaultConfig(BaseConfig):

    # flask-caching setup
    CACHE_TYPE = 'memcached'
    if os.environ.get('PROD'):
        # (Heroku) Connect to memcache with config from environment variables
        CACHE_MEMCACHED_SERVERS = os.environ.get('MEMCACHIER_SERVERS', '')
        CACHE_MEMCACHED_USERNAME = os.environ.get('MEMCACHIER_USERNAME', '')
        CACHE_MEMCACHED_PASSWORD = os.environ.get('MEMCACHIER_PASSWORD', '')
    else:
        CACHE_MEMCACHED_SERVERS = ['127.0.0.1']


    if os.environ.get('PROD'):
        DEBUG = False
        SQLALCHEMY_ECHO = False
    else:
        DEBUG = True
        SQLALCHEMY_ECHO = True

    # heroku (PostgreSQL)
    # if testing local, run command 'export DATABASE_URL=postgres://chris@localhost'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # To create log folder.
    # $ sudo mkdir -p /var/log/ks2
    # $ sudo chown $USER /var/log/ks2
    DEBUG_LOG = '/var/log/ks2/debug.log'

    # Email (Flask-email)
    # https://bitbucket.org/danjac/flask-mail/issue/3/problem-with-gmails-smtp-server

    if os.environ.get('PROD'):
        MAIL_SERVER = 'smtp.sendgrid.net'
        MAIL_PORT = '587'
        MAIL_USERNAME = os.environ.get('SENDGRID_USERNAME', '')
        MAIL_PASSWORD = os.environ.get('SENDGRID_PASSWORD', '')
            # MAIL_USE_TLS = default False
            # MAIL_USE_SSL = default False
            # MAIL_DEBUG = default app.debug
            # MAIL_DEFAULT_SENDER = default None
            # MAIL_MAX_EMAILS = default None
            # MAIL_SUPPRESS_SEND = default app.testing
            # MAIL_ASCII_ATTACHMENTS = default False
        # use production recaptcha keys
        RECAPTCHA_SITE_KEY = "6Lc7TBUUAAAAAG2lcl7Zs0S5aUkmcwgI82kinxgM"
        RECAPTCHA_SECRET_KEY = "6Lc7TBUUAAAAAMU1-PMuJv6jRNNmuLAfp7sjb7uX"
    else: 
        # use gmail as smpt if local
        MAIL_SERVER = 'smtp.googlemail.com'
        MAIL_PORT = 465
        MAIL_USE_TLS = False
        MAIL_USE_SSL = True
        MAIL_USERNAME = os.environ.get('GMAIL_USERNAME')
        MAIL_PASSWORD = os.environ.get('GMAIL_PASSWORD')

        # use recaptcha test keys from https://developers.google.com/recaptcha/docs/faq
        RECAPTCHA_SITE_KEY = "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
        RECAPTCHA_SECRET_KEY = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe"

    MAIL_DEFAULT_SENDER = 'mailer@keepshelf.com'

    # MAIL_DEBUG = DEBUG
    # MAIL_SERVER = 'smtp.gmail.com'
    # MAIL_USE_TLS = True
    # MAIL_USE_SSL = False
    # MAIL_USERNAME = 'chrickso'
    # MAIL_PASSWORD = 'test'
    # DEFAULT_MAIL_SENDER = '%s@gmail.com' % MAIL_USERNAME

    # flask-recaptcha
    # https://github.com/mardix/flask-recaptcha/


    # redis server
    REDIS_URL = os.environ.get('REDISCLOUD_URL', 'redis://localhost:6379/0')
    


class TestConfig(BaseConfig):
    TESTING = True
    CSRF_ENABLED = False

    SQLALCHEMY_ECHO = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_TEST_URL')

    CACHE_MEMCACHED_SERVERS = ['127.0.0.1:11212']
    REDIS_URL = os.environ.get('REDISCLOUD_URL', 'redis://localhost:6379/1')
