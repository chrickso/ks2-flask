# -*- coding: utf-8 -*-

import functools
from flask import request, session

from ks2.utils.next import set_next_in_cookie as set_next_in_cookie_util


def set_next_in_cookie(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        
        set_next_in_cookie_util(request.url)

        return view(**kwargs)

    return wrapped_view

