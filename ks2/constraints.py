# -*- coding: utf-8 -*-

class Constraints(object):

    def __init__(self):
        pass

    # Users

    USER_USERNAME_CHARS_MIN = 3
    USER_USERNAME_CHARS_MAX = 20
    USER_PASSWORD_CHARS_MIN = 6
    USER_PASSWORD_CHARS_MAX = 160 # https://www.owasp.org/index.php/Password_Storage_Cheat_Sheet#Do_not_limit_the_character_set_and_set_long_max_lengths_for_credentials
    USER_EMAIL_CHARS_MAX = 254 # http://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
    USER_IP_CHARS_MAX = 45 # https://stackoverflow.com/a/1076755


    