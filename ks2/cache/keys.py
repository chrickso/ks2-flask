# -*- coding: utf-8 -*-

class CacheKeys(object):

    def __init__(self):
        pass

    # No query results cache string
    NO_QUERY_RESULTS = u'no results'


    # single row
    SINGLE_DETAILS_USER_FROM_ID = u'single_details_user_from_id_%s' # user_id
    SINGLE_DETAILS_USER_FROM_USERNAME = u'single_details_user_from_username_%s' # user_username


    