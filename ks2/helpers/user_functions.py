# -*- coding: utf-8 -*-

from flask import current_app, flash, url_for, render_template, session, redirect
from flask_mail import Message

from werkzeug.routing import RequestRedirect

from ks2.messages import Messages
from ks2.helpers.user_utils import UserUtils
from ks2.utils.payload import PayloadUtils
from ks2.mailers.user_notifications import send_email
from ks2.extensions import mail



message = Messages()
user_util = UserUtils()
payload_util = PayloadUtils()



class UserFunctions(object):

    def __init__(self):
        pass

    
    def send_email_verification_to_user_id(self, user_id):

        user_details = user_util.get_user_details_by_id(user_id)
        serializer = payload_util.get_serializer()
        payload = serializer.dumps(user_details.id, salt=current_app.config['SECURITY_PASSWORD_SALT'])
        verification_link = url_for('usersBP.verify_email', payload=payload, _external=True)

        text_body = render_template('mailer/email_verify/email_verify.txt', verification_link=verification_link)
        html_html = render_template('mailer/email_verify/email_verify.html', verification_link=verification_link)

        mail_send_job = send_email.delay(user_details.email, 'Keepshelf Email Verification', text_body, html_html)

        flash('Verification email sent.', 'success')

        return True



    def process_email_verification_request(self, payload, logged_in_user_id, logged_in_user_email_verified):

        # only proceed if email not already verified
        if not logged_in_user_email_verified == True:

            # check if user is logged in
            if logged_in_user_id:

                # verify the payload is a valid/unexpired payload
                payload_user_id = payload_util.validate_payload(payload)

                # if payload is valid
                if payload_user_id:

                    # if payload is valid and it matches the logged in user_id
                    if payload_user_id == logged_in_user_id:

                        # update the user record as verfified and set email_verified cookie
                        update_user_record = user_util.edit_user_record_email_verified_by_id(logged_in_user_id)
                        update_user_logged_in_cookies = user_util.session_update_user_logs_in_by_user_id(logged_in_user_id)
                        flash('Email is now verified. Thanks!', 'success')

                        # send 'thank you for verifying' email ?

                    else:
                        flash('You are not logged into the account associated with the email you tried to verify.', 'warning')

                else: # payload is invalid

                    flash('The verification link is invalid or has expired.', 'warning')

                redirect_url = url_for('usersBP.profile', user_id=logged_in_user_id)

            else: # user is not logged in

                next = url_for('usersBP.verify_email', payload=payload)            
                redirect_url = url_for('usersBP.login', next=next)
                flash('Please log in to verify your email.', 'warning')

        else: # user has already verified email
            redirect_url = url_for('usersBP.profile', user_id=logged_in_user_id)
            flash('This email has already been verified.', 'success')

        return redirect_url



    def return_user_details_if_logged_in_else_redirect_to_login_url(self):
        user_id = session.get('user_id')
        if user_id:
            user_details = user_util.get_user_details_by_id(user_id)
            return user_details
        else:
            # flash(message.LOG_IN_REQUIRED_TO_VIEW_PAGE)
            raise RequestRedirect(url_for('usersBP.login'))







