# -*- coding: utf-8 -*-

import os

from flask import flash, redirect, request, session, url_for, abort

from ks2.constraints import Constraints
from ks2.messages import Messages
from ks2.models.user_model import UserDB
from ks2.utils.strings import StringFunctions
from ks2.extensions import recaptcha
from ks2.helpers.user_functions import UserFunctions
from ks2.helpers.user_utils import UserUtils



constraint = Constraints()
message = Messages()
string_func = StringFunctions()
user_func = UserFunctions()
user_util = UserUtils()



class UserForms(object):

    def __init__(self):
        pass


    def process_form_signup(self, next=None):

        if next == None:
            next = url_for('pagesBP.homepage')


        # create empty dictionaries
        signup_inputs={}
        signup_errors={}


        # check for bots if on production server
        if os.environ.get('PROD'):
            if not recaptcha.verify():
              signup_errors['captcha'] = message.USER_FORM_PROCESSING_SIGNUP_ERROR_CAPTCHA

        if request.form.get('email_repeat'): # hidden form input that should not be filled in
            abort(404)


        # set empty success_url to return in case not triggered below
        success_url = None


        # set variables of form inputs (and remove whitespaces where appropriate)
        username_input = string_func.no_whitespace(request.form.get('username'))
        # user_email_input = string_func.no_whitespace(request.form.get('email'))
        password_input = request.form.get('password')
        password_repeat_input = request.form.get('password_repeat')

        # if a username was provided, save the no_whitespace'd version to send back to form and check in length is within constraints
        if username_input:
            signup_inputs['username'] = username_input
            if len(username_input) < constraint.USER_USERNAME_CHARS_MIN:
                signup_errors['username'] = message.USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_MIN % constraint.USER_USERNAME_CHARS_MIN
            elif len(username_input) > constraint.USER_USERNAME_CHARS_MAX:
                signup_errors['username'] = message.USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_MAX % constraint.USER_USERNAME_CHARS_MAX
                signup_inputs['username'] = username_input[:constraint.USER_USERNAME_CHARS_MAX]
            # if username is not too long or short, check that it does not contain unsupported characters
            else:
                if signup_inputs['username'] != string_func.sanitize_username(signup_inputs['username']):
                    signup_errors['username'] = message.USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_UNSUPPORTED_CHARS
        else:
            signup_errors['username'] = message.USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_EMPTY
        

        # proceed with input verification if something other than spaces was provided
        

        # verify that a password was provided and it is between 7 and 256 characters
        if password_input:
            if len(password_input) < constraint.USER_PASSWORD_CHARS_MIN:
                signup_errors['password'] = message.USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_MIN % constraint.USER_PASSWORD_CHARS_MIN
            elif len(password_input) > constraint.USER_PASSWORD_CHARS_MAX:
                signup_errors['password'] = message.USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_MAX % constraint.USER_PASSWORD_CHARS_MAX
            else:
                if password_input and not password_repeat_input:
                    signup_errors['password_repeat'] = message.USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_REPEAT_EMPTY
                if password_input and password_repeat_input:
                    if password_input != password_repeat_input:
                        signup_errors['password'] = message.USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_REPEAT_MISMATCH
        else:
            signup_errors['password'] = message.USER_FORM_PROCESSING_SIGNUP_ERROR_PASSWORD_EMPTY

        
        # if no errors up to this point, lookup the username to determine availability
        if not signup_errors:
            username_taken = UserDB.username_taken(username_input)
            if username_taken:
                signup_errors['username'] = message.USER_FORM_PROCESSING_SIGNUP_ERROR_USERNAME_TAKEN
            """
            # use below if email is determined to be used during signup
            email_taken = UserDB.email_taken(user_email_input)
            if email_taken:
                signup_errors['email'] = u'An account with that email address already exists.'
            """

        
        # if no errors, proceed with new user creation
        if not signup_errors:

            # insert new user reocord into UserDB
            created_new_user_status, new_user_id = user_util.db_insert_new_user(username=username_input,
                                                                                password=password_input,
                                                                                creator_ip=request.remote_addr)
            
            # if new user created successfully, set session as logged in and redirect
            if created_new_user_status == True:
                # update cache first incase that username was previous accessed when it didn't exist and has a no-results cache set
                update_user_details_cache = user_util.cache_update_user_details_by_id(new_user_id)
                log_in_user = user_util.session_update_user_logs_in_by_user_id(new_user_id)
                # send_email_verification = user_func.send_email_verification_to_user_id(new_user_id)
                # flash(u'New user created! Please check your email for a verification link.', u'success')
                # flash(u'New user created. You are now logged in as <strong>%s</strong>' % username_input, u'success')
                success_url = url_for('pagesBP.homepage')
            else:
                flash(message.USER_FORM_PROCESSING_SIGNUP_FLASH_ERROR_CREATING_USER, u'error')
        else:
            flash(message.USER_FORM_PROCESSING_SIGNUP_FLASH_ERROR_FORM_ERRORS, u'error')

        return signup_inputs, signup_errors, success_url



    def process_form_login(self, next=None):


        #########################################################################################################
        # need to add a check and test to prevent user from loging in if their account is disabled/inactive
        #########################################################################################################



        # create empty dictionaries
        login_inputs={}
        login_errors={}

        # set empty success_url to return in case not triggered below
        success_url = None

        # set variables of form inputs (and remove whitespaces from username input)
        username_input = string_func.no_whitespace(request.form.get('username'))
        password_input = request.form.get('password')

        # verify something other than spaces was provided
        if username_input:

            # save input to dict in case it's sent back to form
            login_inputs['username'] = username_input

            # verify the username is within the character limits before proceeding
            if len(username_input) < constraint.USER_USERNAME_CHARS_MIN:
                login_errors['username'] = u'Usernames must be at least %s characters' % constraint.USER_USERNAME_CHARS_MIN
            elif len(username_input) > constraint.USER_USERNAME_CHARS_MAX:
                login_errors['username'] = u'Maximum username length is %s characters.' % constraint.USER_USERNAME_CHARS_MAX
                login_inputs['username'] = username_input[:constraint.USER_USERNAME_CHARS_MAX]
            
        else:
            if not username_input:
                login_errors['username'] = u'Username is required to login.'

         # verify that a password was provided and it is within the min/max constraints
        if password_input:
            if len(password_input) < constraint.USER_PASSWORD_CHARS_MIN:
                login_errors['password'] = u'Password must have at least %s characters.' % constraint.USER_PASSWORD_CHARS_MIN
            elif len(password_input) > constraint.USER_PASSWORD_CHARS_MAX:
                login_errors['password'] = u'Password length must not exceed %s characters.' % constraint.USER_PASSWORD_CHARS_MAX
        else:
            login_errors['password'] = u'Password is required to login.'

        # if no errors up to this point, lookup the username/password in UserDB and authenticate
        if not login_errors:
            user_details, authenticated = UserDB.authenticate(username_input, password_input)

            # verify u/p
            if user_details and authenticated:
                user_util.session_update_user_logs_in_by_user_id(user_details.id)
                # flash(u'You are now logged in as <strong>%s</strong>' % username_input, u'success')

                # if a next url was provided, redirect there. otherwise, to user admin
                if next:
                    success_url = next
                else:
                    success_url = url_for('pagesBP.homepage')

            # if verify u/p fails
            else:
                flash(u'That username/password is not in our system.', u'error')

        return login_inputs, login_errors, success_url