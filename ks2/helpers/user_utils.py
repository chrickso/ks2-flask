# -*- coding: utf-8 -*-

from flask import session, flash #, redirect, url_for, request

from ks2.extensions import cache, db
from ks2.models.user_model import UserDB

from ks2.cache.keys import CacheKeys
from ks2.cache.utils import CacheUtils

from ks2.utils.strings import StringFunctions
from ks2.utils.times import TimeUtils


cachekey = CacheKeys()
cache_util = CacheUtils()
string_func = StringFunctions()
time_util = TimeUtils()



class UserUtils(object):

    def __init__(self):
        pass


    # # session updates # #

    def session_update_user_logs_in_by_user_id(self, user_id):

        user_details = self.get_user_details_by_id(user_id)

        session['user_id'] = user_id
        session['user_username'] = user_details.username

        # if user_details.email_verified:
        #     session['email_verified'] = True
        # else:
        #     session['email_verified'] = False

        return True


    def session_update_user_logs_off(self):
        if session.get('user_id'):
            session.pop('user_id', None)
            if session.get('user_username'):
                session.pop('user_username', None)
            # if session.get('email_verified'):
            #     session.pop('email_verified', None)
            flash(u'You are now logged out.', u'success')
        else:
            flash(u'You were not logged in, so you cannot log out.', u'Notice')
        return True


        





    # # database record counts # #




    # # database inserts # #
    
    def db_insert_new_user(self, username=None, password=None, creator_ip=None):
        """Attempts to insert record & returns created_new_user_status, new_user_id"""

        try:
            # in place to test the signup process
            if username == 'kstesttofail':
                return False, None
            else:
                new_user = UserDB(
                                username = username,
                                password = password,
                                creator_ip = creator_ip,
                              )
                db.session.add(new_user)
                db.session.commit()

                return True, new_user.id

        except:

            return False, None




    # # database lookups # #

    def lookup_user_details_by_id(self, user_id):
        results = UserDB.query.get(user_id)
        return results


    def lookup_user_details_by_username(self, user_username):
        results = UserDB.query.filter_by(username=user_username).first()
        return results

    # def lookup_user_is_email_verified(self, user_id):
    #     user_details = self.get_user_details_by_id(user_id)

    #     if user_details.email_verified == True:
    #         return True
    #     else:
    #         return False













    # # database modifications # #

    # def edit_user_record_bla_bla

    # def edit_user_record_email_verified_by_id(self, user_id):
    #     user_details = self.lookup_user_details_by_id(user_id)
    #     user_details.email_verified = True
    #     user_details.email_verified_time = time_util.get_current_time()

    #     db.session.add(user_details)
    #     db.session.commit()

    #     cache_update = self.cache_update_user_details_by_user_details(user_details)

    #     return True









    # # cache gets # #

    # def get_user_details_by_id/username

    def get_user_details_by_id(self, user_id):
        k = (cachekey.SINGLE_DETAILS_USER_FROM_ID % (user_id)).encode('utf-8')
        v = cache.get(k)

        if v:
            v = cache_util.check_cache_for_no_query_results(v)
        else:
            v = self.lookup_user_details_by_id(user_id)
            cache_util.cache_set_results_or_no_results(k, v)
        
        return v


    def get_user_details_by_username(self, user_username):
        sanatized_user_username = string_func.sanitize_username(user_username)
        k = (cachekey.SINGLE_DETAILS_USER_FROM_USERNAME % sanatized_user_username).encode('utf-8')
        v = cache.get(k)

        if v:
            v = cache_util.check_cache_for_no_query_results(v)
        else:
            v = self.lookup_user_details_by_username(sanatized_user_username)
            cache_util.cache_set_results_or_no_results(k, v)
        return v






    # # cache deletes # #

    # def cache_delete_user_details_by_id/email









    # # cache updates # #

    # cache_update_user_details_by_id/email

    def cache_update_user_details_by_id(self, user_id):
        k = (cachekey.SINGLE_DETAILS_USER_FROM_ID % (user_id)).encode('utf-8')
        v = self.lookup_user_details_by_id(user_id)
        cache_util.cache_set_results_or_no_results(k, v)
        return True



    # def cache_update_user_details_by_user_details(self, user_details):
    #     k = (cachekey.SINGLE_DETAILS_USER_FROM_ID % (user_details.id)).encode('utf-8')
    #     v = user_details
    #     cache.set(k, v)
    #     return True





