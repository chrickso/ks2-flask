# -*- coding: utf-8 -*-

## UTILS CANNOT IMPORT OTHER UTILS


# http://flask.pocoo.org/snippets/50/

from flask import current_app, url_for

from itsdangerous import URLSafeTimedSerializer, BadSignature


class PayloadUtils(object):

    def __init__(self):
        pass



    def get_serializer(self, secret_key=None):
        if secret_key is None:
            secret_key = current_app.secret_key
        return URLSafeTimedSerializer(secret_key)



    def get_activation_link(self, user):
        s = self.get_serializer()
        payload = s.dumps(user.id)
        return url_for('activate_user', payload=payload, _external=True)



    def validate_payload(self, token, expiration=172800): # 48 hour expiration
        s = self.get_serializer()
        try:
            user_id = s.loads(
                token,
                salt=current_app.config['SECURITY_PASSWORD_SALT'],
                max_age=expiration
            )
        except:
            return False
        return user_id