# -*- coding: utf-8 -*-

from urlparse import urlparse, urlunparse

from flask import request, session, url_for

def make_next_param(current_url):
    '''
    Reduces the scheme and host from a given URL so it can be passed to
    the given `login` URL more efficiently.
    :param login_url: The login URL being redirected to.
    :type login_url: str
    :param current_url: The URL to reduce.
    :type current_url: str
    '''
    login_url = url_for('usersBP.login')

    l = urlparse(login_url)
    c = urlparse(current_url)

    if (not l.scheme or l.scheme == c.scheme) and \
            (not l.netloc or l.netloc == c.netloc):
        return urlunparse(('', '', c.path, c.params, c.query, ''))
    return current_url


def set_next_in_cookie(current_url):
    session['next'] = make_next_param(request.url)
    return