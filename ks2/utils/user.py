# -*- coding: utf-8 -*-

## UTILS CANNOT IMPORT OTHER UTILS

from ks2.models.user_model import UserDB

class UserUtils(object):

    def check_if_user_exists(self, user_username):
        username_exists = UserDB.username_taken(user_username)

        if username_exists:
            return True
        else:
            return False
            

