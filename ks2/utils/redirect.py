# -*- coding: utf-8 -*-

## UTILS CANNOT IMPORT OTHER UTILS

# Securely Redirect Back
# By Armin Ronacher
# http://flask.pocoo.org/snippets/62/

# note: currently does not work with subdomains

from urlparse import urlparse, urljoin
from flask import request, url_for, redirect

class RedirectUtils(object):

    def __init__(self):
        pass

    def is_safe_url(self, target):
        ref_url = urlparse(request.host_url)
        test_url = urlparse(urljoin(request.host_url, target))
        return test_url.scheme in ('http', 'https') and \
               ref_url.netloc == test_url.netloc

    def get_redirect_target(self):
        for target in request.values.get('next'), request.referrer:
            if not target:
                continue
            if self.is_safe_url(target):
                return target

    def redirect_back(self, endpoint, **values):
        target = request.form.get('next')
        if not target or not self.is_safe_url(target):
            target = url_for(endpoint, **values)
        return redirect(target)

