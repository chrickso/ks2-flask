# -*- coding: utf-8 -*-

## UTILS CANNOT IMPORT OTHER UTILS

# http://flask.pocoo.org/snippets/5/

import re
from unidecode import unidecode

_punct_re = re.compile(r'[\t !"#$%&\'()*\-/<=>?@\[\\\]^_`{|},.:]+')


def slugify(text, delim=u'-'):
    """Generates an ASCII-only slug."""
    result = []
    for word in _punct_re.split(text.lower()):
        result.extend(unidecode(word).split())
    if len(result) > 0:
        return unicode(delim.join(result))
    else:
        return None