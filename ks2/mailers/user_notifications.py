# -*- coding: utf-8 -*-

## UTILS CANNOT IMPORT OTHER UTILS

# https://realpython.com/blog/python/handling-email-confirmation-in-flask/

from flask_mail import Message

from ks2.extensions import mail
from ks2.extensions import rq


@rq.job
def send_email(to, subject, text_body, html_body):

    # create app must be imported inside a job otherwise
    # import will fail due to circular
    from ks2 import create_app
    app = create_app()
    with app.app_context():

        msg = Message(
            subject,
            recipients=[to],
            body=text_body,
            html=html_body
        )
        mail.send(msg)
        return



# reference code
    # msg = Message()

    # msg.recipients = [user_details.email]
    # msg.subject = 'SwapJot Email Verification'
    # msg.html = 'Click to <a href="%(verification_link)s">verify your email address</a>.' \
    #             % { 'verification_link': verification_link }