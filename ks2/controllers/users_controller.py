# -*- coding: utf-8 -*-

from flask import Blueprint, request, render_template, redirect, url_for, session
from ks2.helpers.user_form_processing import UserForms
from ks2.helpers.user_utils import UserUtils


user_form_proc = UserForms()
user_util = UserUtils()


usersBP = Blueprint('usersBP', __name__)


@usersBP.route('/signup', methods=['GET', 'POST'])
def signup():

    # check if already logged in. If yes, redirect to user admin. Else, continue.
    if session.get('user_id'):
        return redirect(url_for('usersBP.profile', user_id=session['user_id']))

    next = url_for('pagesBP.homepage')

    # assign input/error dict for GET requests (required by template)
    signup_inputs={}
    signup_errors={}

    if request.method == 'POST':

        signup_inputs, signup_errors, success_url = user_form_proc.process_form_signup(next=next)

        if success_url:
            return redirect(success_url)

    return render_template('user/signup.html',
                           next=next,
                           signup_inputs=signup_inputs,
                           signup_errors=signup_errors)




@usersBP.route('/login', methods=['GET', 'POST'])
def login():

    # check if already logged in. If yes, redirect to user admin. else, continue.
    if session.get('user_id'):
        return redirect(url_for('usersBP.profile', user_username=session['user_username']))


    # assign input/error dict for GET requests (required by template)
    login_inputs = {}
    login_errors = {}


    if request.method == 'POST':
        
        login_inputs, login_errors, success_url = user_form_proc.process_form_login(next=session.get('next'))

        if success_url:
            return redirect(success_url)

    return render_template('user/login.html',
                       login_inputs=login_inputs,
                       login_errors=login_errors)









@usersBP.route('/user/<user_username>/profile/')
def profile(user_username):
    
    user_details = user_util.get_user_details_by_username(user_username)

    return render_template('user/profile.html', user_details=user_details)




@usersBP.route('/user/<user_username>/settings/')
def settings(user_username):
    
    user_details = user_util.get_user_details_by_username(user_username)

    return render_template('user/profile.html', user_details=user_details)



@usersBP.route('/logout', methods=['POST'])
def logout():

    next = session.get('next')
    redirect_url = next if next else url_for('pagesBP.homepage')
    
    user_util.session_update_user_logs_off()

    return redirect(redirect_url)


# @usersBP.route('/user/email_verify/<payload>')
# def verify_email(payload):

#     redirect_url = user_func.process_email_verification_request(
#                                         payload,
#                                         session.get('user_id'),
#                                         session.get('email_verified')
#                                         )

#     return redirect(redirect_url)






# @usersBP.route('/user/resend_verification_email', methods=['POST'])
# def resend_verification_email():
#     user_id = session.get('user_id')
#     next = redirect_util.get_redirect_target()

#     if user_id:
#         send_email_verification = user_func.send_email_verification_to_user_id(user_id)
#     else:
#         abort(404)

#     return redirect_util.redirect_back('pagesBP.homepage')






