# -*- coding: utf-8 -*-

from flask import Blueprint, render_template


pagesBP = Blueprint('pagesBP', __name__)


@pagesBP.route('/')
def homepage():

    return render_template('pages/homepage.html')