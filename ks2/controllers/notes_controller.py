# -*- coding: utf-8 -*-

from flask import Blueprint, redirect, request, render_template, url_for

from ks2.helpers.note_form_processing import NoteForms
from ks2.helpers.user_functions import UserFunctions

from ks2.decorators import set_next_in_cookie


note_form_proc = NoteForms()
user_func = UserFunctions()


notesBP = Blueprint('notesBP', __name__)


@notesBP.route('/note', methods=['GET', 'POST'])
@set_next_in_cookie
def new_note():

    user_details = user_func.return_user_details_if_logged_in_else_redirect_to_login_url()

    if request.method == 'POST':

        form_inputs, form_errors, success_url = note_form_proc.process_form_new_note()

        if success_url:
            return redirect(success_url)

    return render_template('notes/new_note.html')