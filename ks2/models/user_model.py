# -*- coding: utf-8 -*-

from werkzeug import cached_property
from sqlalchemy.dialects.postgresql import HSTORE, INET
from sqlalchemy.ext.mutable import MutableDict

from ks2.constraints import Constraints
from ks2.utils.times import TimeUtils
from ks2.extensions import db, bcrypt


time_utils = TimeUtils()
constraint = Constraints()


class UserDB(db.Model):

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)

    username = db.Column(db.String(constraint.USER_USERNAME_CHARS_MAX), nullable=False, unique=True, index=True)
    _password = db.Column('password', db.String(constraint.USER_PASSWORD_CHARS_MAX), nullable=False)
    email = db.Column(db.String(constraint.USER_EMAIL_CHARS_MAX), nullable=True)
    email_verified = db.Column(db.Boolean(), nullable=True, default='FALSE')
    email_verified_time = db.Column(db.DateTime, nullable=True)
    creator_ip = db.Column(db.String(constraint.USER_IP_CHARS_MAX), nullable=True)
    created_time = db.Column(db.DateTime, default=time_utils.get_current_time)
    settings = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={})

    def __init__(self, username, password, **kwargs):
        """Create instance."""
        db.Model.__init__(self, username=username, **kwargs)
        if password:
            self._set_password(password)
        else:
            self.password = None


    def _get_password(self):
        return self._password

    def _set_password(self, password):
        self._password = bcrypt.generate_password_hash(password)

    # Hide password encryption by exposing password field only.
    password = db.synonym('_password',
                          descriptor=property(_get_password,
                                              _set_password))

    def check_password(self, password):
        if self.password is None:
            return False
        return bcrypt.check_password_hash(self.password, password)


    @classmethod
    def authenticate(cls, username, password):
        user = cls.query.filter(db.func.lower(UserDB.username)==db.func.lower(username)).first()

        if user:
            authenticated = user.check_password(password)
        else:
            authenticated = False

        return user, authenticated


    @classmethod
    def username_taken(cls, username):
        u = cls.query.filter(db.func.lower(UserDB.username)==db.func.lower(username)).first()

        if u:
            return True
        else:
            return False


    